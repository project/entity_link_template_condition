Entity Link Template Condition

https://www.drupal.org/project/entity_link_template_condition

This project provides a condition which matches when the current request
represents a route of an entity link template. The condition works with any
entity which uses link templates.

For example you can match when you are on a Node view page (canonical), or
Edit Form (edit-form), etc.

[Entity Route Context][entity-route-context] project provides an all purpose
context which activates when you're on a route for an entity.

[entity-route-context]: https://www.drupal.org/project/entity_route_context

# License

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin 
Street, Fifth Floor, Boston, MA 02110-1301 USA.
