<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_link_template_condition\Kernel;

use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\entity_link_template_condition\Plugin\Condition\EntityLinkTemplateCondition;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Routing\Route;

/**
 * @group entity_link_template_condition
 */
final class EntityLinkTemplateConditionTest extends KernelTestBase {

  protected static $modules = [
    'entity_link_template_condition',
    'entity_route_context',
    'entity_test',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
  }

  /**
   * Tests the condition.
   */
  public function testCondition(): void {
    $entity = EntityTest::create(['name' => $this->randomString()]);
    $entity->save();

    /** @var \Drupal\Core\Condition\ConditionInterface|\Drupal\Core\Plugin\ContextAwarePluginInterface $condition */
    $condition = $this->container->get('plugin.manager.condition')->createInstance(EntityLinkTemplateCondition::PLUGIN_ID);
    $this->assertFalse($condition->evaluate());
    $condition->setConfiguration([
      'link_templates_any' => ['canonical'],
    ]);
    $this->assertFalse($condition->evaluate());
    $this->setCurrentRequestForRoute('/entity_test/{entity_test}', 'entity.entity_test.canonical');
    $this->assertTrue($condition->evaluate());
  }

  /**
   * Sets the current request to a specific path with the corresponding route.
   *
   * @param string $path
   *   The path for which the current request should be created.
   * @param string $route_name
   *   The route name for which the route object for the request should be
   *   created.
   */
  protected function setCurrentRequestForRoute(string $path, string $route_name): void {
    $request = Request::create($path);
    $request->attributes->set(RouteObjectInterface::ROUTE_NAME, $route_name);
    $request->attributes->set(RouteObjectInterface::ROUTE_OBJECT, new Route($path));
    $request->setSession(new Session(new MockArraySessionStorage()));
    $this->container->get('request_stack')->push($request);
  }

}
