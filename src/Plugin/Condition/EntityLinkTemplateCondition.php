<?php

declare(strict_types=1);

namespace Drupal\entity_link_template_condition\Plugin\Condition;

use Drupal\Core\Condition\Attribute\Condition;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_route_context\EntityRouteContextRouteHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides condition for filtering current request on link templates.
 */
#[Condition(
  id: self::PLUGIN_ID,
  label: new TranslatableMarkup('Entity Link Template'),
)]
class EntityLinkTemplateCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  public const PLUGIN_ID = 'entity_link_template';

  /**
   * Constructs a EntityLinkTemplateCondition condition plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Current route match.
   * @param \Drupal\entity_route_context\EntityRouteContextRouteHelperInterface $routeHelper
   *   Entity Route Context route helper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RouteMatchInterface $routeMatch,
    protected EntityRouteContextRouteHelperInterface $routeHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('entity_route_context.route_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'link_templates_any' => [],
      'link_templates' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $linkTemplates = $this->getLinkTemplates();
    \asort($linkTemplates);
    $form['link_templates_any'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('When the current request is a link template for any entity type'),
      '#default_value' => $this->configuration['link_templates_any'],
      '#options' => \array_combine($linkTemplates, \array_map(function (string $linkTemplate) {
        return $this->t('@link_template', ['@link_template' => $linkTemplate]);
      }, $linkTemplates)),
    ];

    $byEntityTypeOptions = [];
    foreach ($this->getLinkTemplatesByEntityType() as $entityTypeId => $linkTemplates) {
      foreach ($linkTemplates as $linkTemplateKey => $label) {
        $byEntityTypeOptions[$entityTypeId . ':' . $linkTemplateKey] = $label;
      }
    }
    \asort($byEntityTypeOptions);
    $form['link_templates_entity_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('When the current request for link template matches an exact entity type'),
      '#default_value' => $this->configuration['link_templates'],
      '#options' => $byEntityTypeOptions,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['link_templates_any'] = \array_values(\array_filter($form_state->getValue('link_templates_any')));
    $this->configuration['link_templates'] = \array_values(\array_filter($form_state->getValue('link_templates_entity_type')));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('Link templates: @link_templates', [
      '@link_templates' => $this->configuration['link_templates'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $anyLinkTemplateKey = $this->configuration['link_templates_any'];
    $entityTypeLinkTemplateKey = $this->configuration['link_templates'];
    if (empty($anyLinkTemplateKey) && empty($entityTypeLinkTemplateKey)) {
      return FALSE;
    }

    [$entityTypeId, $linkTemplateKey] = $this->routeHelper->getLinkTemplateByRouteMatch($this->routeMatch);
    if ($entityTypeId) {
      if (\in_array($linkTemplateKey, $anyLinkTemplateKey, TRUE)) {
        return TRUE;
      }

      if (\in_array(\sprintf('%s:%s', $entityTypeId, $linkTemplateKey), $entityTypeLinkTemplateKey, TRUE)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Get all link templates.
   *
   * @return string[]
   *   Link templates.
   */
  public function getLinkTemplates(): array {
    $linkTemplates = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entityType) {
      \array_push($linkTemplates, ...\array_keys($entityType->getLinkTemplates()));
    }
    return \array_unique($linkTemplates);
  }

  /**
   * Get link templates for all entity types.
   *
   * @return array
   *   Arrays of link templates keyed by entity type ID.
   */
  public function getLinkTemplatesByEntityType(): array {
    $linkTemplates = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entityType) {
      $linkTemplateKeys = \array_keys($entityType->getLinkTemplates());
      $linkTemplates[$entityType->id()] = \array_map(
        function (string $linkTemplateKey) use ($entityType) {
          return $this->t('@entity_type: @link_template_key', [
            '@entity_type' => $entityType->getLabel(),
            '@link_template_key' => $linkTemplateKey,
          ]);
        },
        \array_combine($linkTemplateKeys, $linkTemplateKeys),
      );
    }
    return \array_filter($linkTemplates);
  }

}
